import argparse
import requests
import json

jisho_url = "http://jisho.org/api/v1/search/words?keyword={}&page={}"
anki_connect_url = "http://localhost:8765"


def _make_request_and_convert_response(query_string, query_data=None):
    """
    
    :param str url: 
    :param None|dict query_data: 
    :return: 
    """
    print("Sending request to {}".format(query_string))
    if query_data is None:
        response = requests.post(query_string)
    else:
        print("Using data {}".format(query_data))
        stringed_data = json.dumps(query_data)
        response = requests.post(query_string, data=stringed_data)
    print("Request successful")
    return json.loads(response.content)


def fetch_from_jisho(keyword, page_number):
    """ Run a get request to Jisho using the given search keyword. Return the json given back by the api
    
    :param str keyword: 
    :param int page_number: 
    :return: list of dicts, query response
    """
    query_string = jisho_url.format(keyword, page_number)
    response = _make_request_and_convert_response(query_string)
    if response['meta']['status'] != 200:
        print("Jisho.org raised unexpected error: {}".format(response))
    return response['data']


def create_anki_notes(deck_name, jisho_list):
    """
    
    :param str deck_name: 
    :param list[dict] jisho_list: 
    :return: 
    """
    notes = []
    for jisho_entry in jisho_list:
        # we want to make three entries: kanji -> reading+meaning. kanji_reading->meaning, and meaning->kanji+reading
        kanji_str = jisho_entry["japanese"][0].get("word")
        reading_str = jisho_entry["japanese"][0]["reading"]
        meaning_str = ""
        num_senses = len(jisho_entry["senses"])
        for i, sense in enumerate(jisho_entry["senses"]):
            meaning_str += sense["english_definitions"][0]
            if i != num_senses - 1:
                meaning_str += ", "

        if kanji_str is not None:
            front_and_back_strings = [(kanji_str, reading_str + ", " + meaning_str),
                                      (kanji_str + ", " + reading_str, meaning_str),
                                      (meaning_str, kanji_str + ", " + reading_str)]
        else:
            front_and_back_strings = [(reading_str, meaning_str),
                                      (meaning_str, reading_str)]

        for front_string, back_string in front_and_back_strings:
            notes.append({
                "deckName": deck_name,
                "modelName": "Basic",  # what's this mean
                "fields": {
                    "Front": front_string,
                    "Back": back_string
                },
                "tags": []
            })

    query_data = {
        "action": "addNotes",
        "version": 6,
        "params": {
            "notes": notes
        }
    }
    add_notes_response = _make_request_and_convert_response(anki_connect_url,
                                                            query_data=query_data)
    if add_notes_response["error"] is None:
        print "Created note successfully with ID {}".format(add_notes_response["result"])
    else:
        print "Error in note creation: {}".format(add_notes_response["error"])


def push_jisho_definition_to_ankiconnect(jisho_list, deck_name):
    """ Given a dict representing a Jisho.org dictionary entry, create an Anki card and push it to the local AnkiConnect
    server under the specified deck name.
    
    :param list[dict] jisho_list:
    :param str deck_name:
    :return: 
    """
    # check if the deck already exists
    deck_names_dict = _make_request_and_convert_response(anki_connect_url,
                                                         query_data={"action": "deckNamesAndIds", "version": 6})
    deck_names = deck_names_dict["result"]
    print("Deck names: {}".format(deck_names))
    if deck_name in deck_names:
        deck_id = deck_names[deck_name]
        print("Deck already exists with ID {}, skipping creation.".format(deck_id))
    else:
        print("Deck does not exist, creating...")
        response = _make_request_and_convert_response(anki_connect_url,
                                                      query_data={"action": "createDeck",
                                                                  "version": 6,
                                                                  "params":
                                                                      {
                                                                          "deck": deck_name
                                                                      }
                                                                  })
        deck_id = response["result"]
        print("Created deck with ID {}".format(deck_id))
    # we now know we have the deck, and have its ID
    create_anki_notes(deck_name, jisho_list)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Fetch vocabulary from jisho using the given parameters and convert'
                                                 'them to a new anki deck. Requires an active internet connection and'
                                                 'a locally running AniConnect server.')
    parser.add_argument('-k', '--keyword', type=str,
                        help='List of strings to use in the search.')
    parser.add_argument('-p' '--pages', type=int, default=1,
                        help='Number of pages to request from jisho.')
    parser.add_argument('-n', '--name', type=str, default='jisho_to_anki_default',
                        help='Name of output anki deck.')

    args = parser.parse_args()
    if args.keyword is None:
        raise ValueError("Required input missing: -keyword")
    pages = args.p__pages  # who knows

    for page_number in range(1, pages + 1):
        print("Requesting page {} of {} from jisho.org".format(page_number, pages))
        jisho_list = fetch_from_jisho(args.keyword, page_number)
        print("Creating notes for page {} of {}".format(page_number, pages))
        push_jisho_definition_to_ankiconnect(jisho_list, args.name)
