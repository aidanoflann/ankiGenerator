This is a simple python script which can be used to generate Anki decks from Jisho.org searches.

It requires a connection to the internet, and a locally running AnkiConnect server (Ankiconnect is an easy-to-install
Anki plugin that can be found here https://ankiweb.net/shared/info/2055492159). The expected AnkiConnect port is 8765.

An example usage of the script is as follows:

```
python jisho_to_anki.py -k %23jlpt-n4 -p 29 -n JishoN4
```

Here, the arguments used are:

-k or --keyword: the keyword to use for the Jisho.org search. In this case it is #jlpt-n4, which will filter the search
such that it only includes JLPT N4-level vocabulary.

-p or --pages: the number of pages to iterate over. This means that, if a certain query returns more than one page,
the script will continue to iterate to the next page, and continue creating Anki deck entries, up until the provided
limit.

-n or --name: the name of the deck to place the new cards into. If a deck with the given name does not exist, one is
created.